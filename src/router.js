// IMPORTACION DE ELEMENTOS BASICOS 

import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

// USO DEL VUE ROUTER PARA POSIBLES RUTAS
Vue.use(Router)

export default new Router({
  routes: [
    // RUTAS DEL PROGRAMA
    {
      path: '/',
      name: 'home',
      component: Home
    }, 
  ]
})
