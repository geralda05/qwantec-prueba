// IMPORTACION DE ELEMENTOS BASICOS DE VUE

import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false;

// EXPORTAR ELEMENTOS O COMPILAR
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
